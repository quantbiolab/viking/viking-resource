from argparse import ArgumentParser
import json
from logging import getLogger
import os
from yaml import safe_load
from hashlib import sha256
import traceback

import sys

import os
import boto3
import botocore

from viking_resource import S3
from viking_resource.config import Config
from viking_resource.jobs import fill_file_path

logger = getLogger('viking')

def main(config: Config, args):
    data = json.loads(args)

    program_slug = data['program']

    with open(os.path.join(config.commands_path, program_slug, 'run.yaml')) as f:
        program_data = safe_load(f)
        
    if 'program_cmd' in data and data['program_cmd']:
        program_data = program_data[data['program_cmd']]

    if 'hash' not in program_data:
        return
    
    os.makedirs(data['cwd'], exist_ok=True)

    s3 = S3(config)

    if 'download' in program_data:
        for file in program_data['download']:
            if 'format_fields' in program_data:
                file = fill_file_path(file, program_data['format_fields'], data['format_data'])
            path = os.path.join(data['task_dir'], file)
            s3.download(path, overwrite=True)
    else:
        s3.download(data['task_dir'], overwrite=True)
    
    md = sha256()
    for file in program_data['hash']:
        if 'format_fields' in program_data:
            file = fill_file_path(file, program_data['format_fields'], data['format_data'])
        path = os.path.join(data['task_dir'], file)
        with open(path, 'rb') as f:
            for line in f:
                md.update(line)
    
    print(md.hexdigest())

if __name__ == '__main__':
    config_file = sys.argv[1]
    arg_json = ' '.join(sys.argv[2:])

    logger.debug(arg_json)
          
    config = Config(config_file)

    try:
        main(config, arg_json)
    except Exception as e:
        logger.error(f'Program hash failed with exception {e}')
        logger.debug(traceback.format_exc())
