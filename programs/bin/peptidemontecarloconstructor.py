#!/usr/bin/python3

import math
import numpy
import random
import shutil
import os
import getopt
import sys
import time


class Configuration:
    """Save the config data"""
    def naming(self):
        for i in range (0,len(self.confile)):
            if "name:" in self.confile[i]:
                trash, name = self.confile[i].split(":")
                #self.confile.pop(i)
                return name[:-1]
        print("Error: Project needs to be named!")
        os._exit(1)
    def adding(self):
        for i in range (0,len(self.confile)):
            if "adding:" in self.confile[i]:
                trash, adding = self.confile[i].split(":")
                #self.confile.pop(i)
                adding=int(adding[:-1])
                return adding
        return 0
    def structuring(self):
        if self.add==0:
            return 0
        for i in range (0,len(self.confile)):
            if "structure:" in self.confile[i]:
                trash, name = self.confile[i].split(":")
                name=name[:-1]
                #self.confile.pop(i)
                if os.path.exists(name):
                    return name
        if self.add==1:
            print("Error: Structure file does not exsist in working directory. Reminder: Your working directory is: "+self.cwd)
            os._exit(1)
        return "NotAStructure"
    def chaining(self):
        for i in range (0,len(self.confile)):
            if "chain:" in self.confile[i]:
                trash, chain = self.confile[i].split(":")
                chain=chain[:-1]
                #self.confile.pop(i)
                chainlist=[]
                for char in chain:
                    if char.isalpha:
                        chainlist.append(char.upper())
                #chain=chain[:-1]
                #chainlist=chain.split(" ")
                chaindict=list(set(chainlist))
                AvailableResidues=("A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X","Y")
                for item in chaindict:
                    if not item in AvailableResidues:
                        print("Error: A non admissable amino acid was added to the sequence! (Bad item is "+item+")")
                        os._exit(1)
                if self.add==0 and len(chainlist)==0:
                    print("This would generate an empty pdb file. Please enter at least one amino acid in your sequence.")
                    os._exit(1)
                return chainlist
        return ""
    def secondaring(self):
        for i in range (0,len(self.confile)):
            if "secondary:" in self.confile[i]:
                trash, structure = self.confile[i].split(":")
                #self.confile.pop(i)
                structure=structure[:-1]
                if self.add==1 and "NotAStructure" in structure:
                    print("Error: You need to supply a pdb structure file to be appended!")
                    os._exit(1)
                if structure=="":
                    structure="C"
                structurelist=[]
                for char in structure:
                    if char.isalpha:
                        structurelist.append(char.upper())
                structuredict=list(set(structurelist))
                AvailableSecondary=("C","H","E")
                for item in structuredict:
                    if not item in AvailableSecondary:
                        print("Error: A non admissable secondary structure was supplied!")
                        os._exit(1)
                return structurelist
    def vegasing(self):
        for i in range (0,len(self.confile)):
            if "vegas:" in self.confile[i]:
                trash, vegas = self.confile[i].split(":")
                vegas=vegas[:-1]
                #self.confile.pop(i)
                return int(vegas)
        return 0
    def running(self):
        for i in range (0,len(self.confile)):
            if "runs:" in self.confile[i]:
                trash, runs = self.confile[i].split(":")
                runs=runs[:-1]
                #self.confile.pop(i)
                return int(runs)
        return 1
    def chainnumbering(self):
        for i in range (0,len(self.confile)):
            if "chainnumber:" in self.confile[i]:
                trash, chainnum, = self.confile[i].split(":")
                chainnum=chainnum[:-1]
                if chainnum=="default":
                    chainnum=""
                #self.confile.pop(i)
                return chainnum
        return ""
    def cutoffing(self):
        for i in range (0,len(self.confile)):
            if "cutoff:" in self.confile[i]:
                trash, cutoff, = self.confile[i].split(":")
                cutoff=cutoff[:-1]
                return int(cutoff)
                #self.confile.pop(i)
        return 0

    def __init__(self,configfile):
        self.cwd=os.getcwd()
        file=open(configfile,"r")
        self.confile=file.readlines()
        self.name=self.naming()
        self.add=self.adding()
        self.structure=self.structuring()
        self.chain=self.chaining()
        self.secondary=self.secondaring()
        if self.secondary==0:
            self.secondary=[]
        while len(self.secondary)<len(self.chain):
                    self.secondary=self.secondary+["C"]
        self.vegas=self.vegasing()
        self.runs=self.running()
        self.chainnumber=self.chainnumbering()
        self.boolcutoff=self.cutoffing()
        file.close

class Parameters:
    """Parameters are basically a class to allow for some global variables in a controlled fashion. It saves data,
    which is needed for several checks along the way, including but not limited to, the accepted possibilities for
    amino acids, the restart coordinates for a good translation or the counter for the steps and therefore the
    termination of Monte Carlo and Las Vegas."""

    def Counter(self):
        """This counter serves as the terminator for the runs. It is done here in Parameters, so the script globally knows
        what run it is on. This is needed to write output files etc."""
        self.step = self.step + 1
        if self.MonteCarlo == 1:
            return self.step
        else:
            if self.Traj == self.steps:
                return self.steps
            else:
                return self.step

    def __init__(self):
        """This is basically just a list of things one needs to remember and wants to know at different places in the program.
        e.g. the program name is needed to write the appopriate output files, or the AvailList is an easy way to maybe even
        update the script with non standard amino acids. Things that need to be remembered over the course of multiple iterations
        is saved here. Just to be save."""
        self.name = "Name"
        self.pdb = ""
        self.ResChain = []
        self.steps = 1
        self.step = 1
        self.AvailList = (
            "A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
            "Y")
        self.RunningNewStart = (0.0, 0.0, 0.0)
        self.iteration = 0
        self.Value = 1
        self.MonteCarlo = 1
        self.Traj = 0
        self.NumRes = 0
        self.pdbstart = (0, 0, 0)
        self.addpdb = 0
        self.psf = 0
        self.StructureList = []
        self.linenumber=0
        self.chain=""
        self.starttime=time.perf_counter()


class Amino:

    def WriteCoords(self, Output):
        """Writes the lines to the file to the output file"""
        for i in range(1, len(self.Lines) - 1):
            Output.write(self.Lines[i])

    def move(self, xyz, sign):
        """Translates the whole amino acid by a vector. If sign=1, it will subtract, if sign=0 it will add
        the vector. Then the coordinates of the Amino Acid are overwritten by the new position"""
        for i in range(0, len(self.Coords)):
            self.Coords[i] = numpy.asarray(self.Coords[i]) + math.pow(-1, sign) * numpy.asarray(xyz)

    def writelines(self):
        """The lines of the original pdb are kept in the self.Lines. These lines will be added, or replaced by this
        procedure to keep the right format of the pdb, when the lines are written to the output file. Writelines
        makes sure, that all distances are kept and that the right amount of spaces is seen between the parts of
        the pdb."""
        for i in range(1, len(self.Lines) - 1):
            Full = ""
            for j in range(0, 3):
                Coordsstring = str(self.Coords[i - 1][j])
                if self.Coords[i - 1][j] >= 0:
                    Coordsstring = "_" + Coordsstring
                split = Coordsstring.split(".")
                while len(split[1]) > 3:
                    split[1] = split[1][:-1]
                while len(split[1]) < 3:
                    split[1] = split[1] + "0"
                while len(split[0]) < 3:
                    split[0] = " " + split[0]
                Full = Full + split[0] + "." + split[1] + " "
                while "   " in Full:
                    Full = "  ".join(Full.split("   "))
            FullFinal = Full.replace("_", " ")
            Parameters.linenumber=Parameters.linenumber+1
            linenumber=str(Parameters.linenumber)
            while len(linenumber)<8:
                   linenumber=" "+linenumber
            self.Lines[i] = self.Lines[i][:31] + FullFinal + self.Lines[i][-24:]
            self.Lines[i]=self.Lines[i][:21]+PDB.chain+self.Lines[i][22:72]+linenumber+"\n"

    def writeresid(self, j):
        """In Order to write the right resid to the lines, the resid is taken from the loop we are in and then
        written to the lines. Here the distance and amount of spaces is already taken care of"""
        self.resid = j + PDB.LastID
        id = str(self.resid)
        while len(id) < 3:
            id = " " + id
        for i in range(1, len(self.Lines) - 1):
            self.Lines[i] = self.Lines[i][:23] + id + self.Lines[i][26:]

    def getNext(self):
        """The next start point is calculated. This is a basic geometric summation of vectors."""
        VectorC = self.Coords[2]
        VectorCA = self.Coords[1]
        VectorO = self.Coords[3]
        VectorC = numpy.asarray(VectorC)
        VectorCA = numpy.asarray(VectorCA)
        VectorO = numpy.asarray(VectorO)
        # Calculating new Coordinates
        new = VectorO - VectorCA + VectorC + VectorC - VectorO + VectorC - VectorO
        return new

    def getAxis(self, Point2, Point1):
        """getAxis gets the turn axis for the randomized turnes along phi, psi and for the side chain."""
        Support = numpy.asarray(Point1)
        Second = numpy.asarray(Point2)
        Direction = Support - Second
        normal = numpy.divide(Direction, math.sqrt(numpy.dot(Direction, Direction)))
        return normal

    def Turn(self, First, Second, writestart, writeend, angle):
        """Does the random turns for phi, psi and the sidechains, if called. It takes two points (First and Second),
        which will be used to construct the axis on which it will turn. writestart and writeend are used, so only the
        non rigid points during the turn get updated in the Amino Acids Coordinates."""
        n = self.getAxis(self.Coords[First], self.Coords[Second])
        self.move(self.Coords[Second], 1)
        Matrix = Drehmatrix(n, angle)
        for i in range(writestart, writeend):
            self.Coords[i] = numpy.dot(Matrix, self.Coords[i])
        self.move(self.Coords[0], 1)

    def PhiandPsi(self):
        """BLUEGOOD and REDGOOD contain the allowed pairs of phi and psi to be in the blue or red areas in the Ramachandran plot
        Then according to the predefined secondary structures, angles are chosen. It takes care of the secondary structure in terms of
        it takes ideal angels, if necessary. If we are in a coil state in randomizes in the stables areas."""
        Odds = random.randint(1, 100)
        if Odds > 90 and self.structure == "C":  # This number needs to be determined (5% hit blue)
            file = open(PATH+"AminoAcids/BLUEGOOD", "r")
        else:
            file = open(PATH+"AminoAcids/REDGOOD", "r")
        Valid = []
        lines = file.readlines()
        for i in range(0, len(lines)):
            if len(lines[i]) > 4:
                Valid.append(1)
                lines[i] = lines[i].rstrip()
                floats = lines[i][1:-1].split(",")
                for j in range(0, len(floats)):
                    floats[j] = float(floats[j])
            else:
                Valid.append(0)
                floats = lines[i].split(",")
            lines[i] = floats
        if self.structure == "H":
            self.phi = -57 - self.phiold
            self.psi = -47 - self.psiold
        elif self.structure == "E":
            self.phi = -139 - self.phiold
            self.psi = 135 - self.psiold
        else:
            Chooser = 0
            while Chooser == 0:
                self.phi = random.randint(-180, 179)
                self.phi = self.phi - 180
                if self.phi < -180:
                    self.phi = self.phi + 360
                Chooser = Valid[self.phi]
            self.psi = lines[self.phi][random.randint(0, len(lines[self.phi]) - 1)]
            self.psi = int(self.psi) - self.psiold
            self.phi = self.phi - self.phiold

        # print(str(self.phi) + "/" + str(self.psi) + ",", end='')
        # print("Phi: "+str(self.phi)+" and Psi: "+str(self.psi))

        file.close()

    def Alter(self):
        """Alter initiates the randomized turns one after the other. It also checks, if a sidechain even exists,
        if not, it will not randomize the side chain turn."""
        if self.name != "P":  # Proline is different and big, leaving it unaltered for the time being
            sidechain = random.randint(0, 359)
            # if self.name != "G":  # Glysine is also weird
            self.PhiandPsi()
            self.Turn(0, 1, 1, len(self.Coords), self.phi)
            self.Turn(1, 2, 2, 3, self.psi)
            if len(self.Coords) >= 5:
                self.Turn(1, 4, 4, len(self.Coords), sidechain)
            self.Newstart = self.getNext()

    def getcenter(self):
        """To save calculations, getcenter gets the geometric center of an Amino Acid. This is used in the
        getValue function"""
        xyz = [0, 0, 0]
        Divisor = 0
        for i in range(0, len(self.Coords)):
            for j in range(0, 3):
                xyz[j] = xyz[j] + self.Coords[i][j]
            Divisor = Divisor + 1
        for i in range(0, 3):
            xyz[i] = (xyz[i] / Divisor)
        return xyz

    def __init__(self, name):
        """When the Amino Acid is initalized, it only needs the one letter notation for the Amino Acid. The script
        will then read the appropriate file from the AminoAcids/ Folder."""
        self.name = name
        self.Coords = []
        self.first = 0
        PDBfile = open(PATH+"AminoAcids/" + self.name + "_normalized.pdb", "r")
        self.Lines = PDBfile.readlines()
        self.atoms=[]
        # Some Iterations have been made, unsure which of the following variables are actually being used:
        for i in range(0, len(self.Lines)):
            if "ATOM" in self.Lines[i][:6]:
                self.atoms.append(self.Lines[i][13:16])
                coordstr=self.Lines[i][32:-26]
                while "  " in coordstr:
                    coordstr=coordstr.replace("  "," ")
                self.Coords.append(list(filter(None,coordstr.split(" "))))
                self.Coords=list(filter(None, self.Coords))

                for j in range(0, 3):
                    self.Coords[-1][j] = float(self.Coords[-1][j])
        self.Newstart = self.getNext()
        self.resid = 1
        self.processed = 0
        self.phi = random.randint(0, 359)
        self.psi = random.randint(0, 359)
        self.structure = "D"
        self.phiold = 0
        self.psiold = 0


class PDB:
    """fill is the second initializing step for the pdb class, it takes care of constructing the amino acids, that
    occur in the pdb and adding them to the Amino class. That way they can be considered, when distances are calculated.
    Additionally, the last residue in the pdb gets it's own name, because it will actually be needed for the Planize
    function."""
    def fill(self):
        file = open(Parameters.pdb, "r")
        self.lines = file.readlines()
        self.chain=Parameters.chain
        if self.chain=="":
            for line in self.lines:
                if "ATOM" in line:
                    self.chain=line[21]
                    break
        resids=[]
        for line in self.lines:
            if "ATOM" in line[:6] and self.chain in line[20:22]:
                resids.append(int(line[22:26]))
        self.LastID=max(resids)
        ############################################################   
        #Replaced Section to find highest ResID instead of last residue as last residue
        """
        for j in range(1, len(self.lines)):
            i = len(self.lines) - 1 - j
            if "ATOM" in self.lines[i][:6]:
                self.LastID = int(self.lines[i][22:26])
                self.chain=self.lines[i][21]
                if self.LastID != 0:
                    break
        """
        ############################################################    
        lastfile = open(PATH+"AminoAcids/Last_normalized.pdb", "w")
        lastfile.write("First Line of the Last File to conform to standards\n")
        for i in range(0, len(self.lines)):
            if str(self.LastID) in self.lines[i][22:26]:
                if " OT1 "in self.lines[i]:
                    Constructor=self.lines[i].split(" OT1 ")
                    Build=Constructor[0]+" O   "+Constructor[1]
                    lastfile.write(Build)
                elif " OT2 " in self.lines[i]:
                    pass
                else:
                    lastfile.write(self.lines[i])
        lastfile.write("END")
        lastfile.close()

        self.Last = Amino("Last")
        Newstart()
        os.remove(PATH+"AminoAcids/Last_normalized.pdb")
        FirstID=min(resids)
        Aminocount = FirstID
        workfile=open(PATH+"AminoAcids/" + str(Aminocount) + "_normalized.pdb", "w")
        workfile.write("First Line of the File to conform to standards\n")
        for i in range (0, len(self.lines)):
            if "ATOM" in self.lines[i][:6] and self.chain in self.lines[i][20:24]:
                if Aminocount == int(self.lines[i][22:26]):
                    workfile.write(self.lines[i])
                if Aminocount > int(self.lines[i][22:26]):
                    Aminocount=Aminocount+1
                    workfile.close()
                    self.CompleteList.append(Amino(str(Aminocount)))
                    os.remove(PATH+"AminoAcids/" + str(Aminocount) + "_normalized.pdb")
                    workfile = open(PATH+"AminoAcids/" + str(Aminocount) + "_normalized.pdb", "w")
                    workfile.write("First Line of the File to conform to standards\n")
                    workfile.write(self.lines[i])
        workfile.write("END")
        workfile.close()
        self.CompleteList.append(self.Last)
        """
        while Aminocount != self.LastID:
            print(self.LastID)
            workfile = open(PATH+"AminoAcids/" + str(Aminocount) + "_normalized.pdb", "w")
            workfile.write("First Line of the File to conform to standards\n")
            for i in range(0, len(self.lines)):
                if "ATOM" in self.lines[i][:6]:
                    if Aminocount == int(self.lines[i][22:26]):
                        workfile.write(self.lines[i])
                    if Aminocount > int(self.lines[i][22:26]):
                        break
            workfile.write("END")
            workfile.close()
            self.CompleteList.append(Amino(str(Aminocount)))
            os.remove(PATH+"AminoAcids/" + str(Aminocount) + "_normalized.pdb")
            Aminocount = Aminocount + 1
        self.CompleteList.append(self.Last)
        """
    """The Init takes care of the bare neccesities here"""

    def __init__(self):
        self.LastID = 0
        self.CompleteList = []
        self.chain=" "


def Newstart():
    file = open(PATH+"AminoAcids/Last_normalized.pdb", "r")
    file_lines = file.readlines()
    OCoords = [0, 0, 0]
    CCoords = [0, 0, 0]
    CACoords = [0, 0, 0]
    for i in range(1, len(file_lines) + 1):
        j = len(file_lines) - i
        if "ATOM" in file_lines[j][:6]:
            if " O " in file_lines[j][:18]:
                OCoords = file_lines[j][32:-26].split(" ")
                OCoords=list(filter(None,OCoords))
            if " C " in file_lines[j][:18]:
                CCoords = file_lines[j][32:-26].split(" ")
                CCoords=list(filter(None,CCoords))
            if " CA " in file_lines[j][:18]:
                CACoords = file_lines[j][32:-26].split(" ")
                CACoords=list(filter(None,CACoords))
                break
    for i in range(0, 3):
        OCoords[i] = float(OCoords[i])
        CCoords[i] = float(CCoords[i])
        CACoords[i] = float(CACoords[i])
    VectorO = numpy.asarray(OCoords)
    VectorCA = numpy.asarray(CACoords)
    VectorC = numpy.asanyarray(CCoords)
    Parameters.pdbstart = VectorO - VectorCA + VectorC + VectorC - VectorO + VectorC - VectorO


def getcenter(Amino):
    """To save calculations, getcenter gets the geometric center of an Amino Acid. This is used in the
    getValue function"""
    xyz = [0, 0, 0]
    Divisor = 0
    for i in range(0, len(Amino.Coords)):
        for j in range(0, 3):
            xyz[j] = xyz[j] + Amino.Coords[i][j]
        Divisor = Divisor + 1
    for i in range(0, 3):
        xyz[i] = (xyz[i] / Divisor)
    return xyz


def Drehmatrix(Axis, angle):
    """Drehmatrix is german for rotation matrix, this procedure takes an axis and an angle to create a matrix
    that one can multiply with to turn a point by the angle around the axis."""
    angle = math.radians(angle)
    # Generating the Turning Matrix:
    a11 = (Axis[0] * Axis[0] * (1 - math.cos(angle))) + (math.cos(angle))
    a12 = (Axis[0] * Axis[1] * (1 - math.cos(angle))) - (Axis[2] * math.sin(angle))
    a13 = (Axis[0] * Axis[2] * (1 - math.cos(angle))) + (Axis[1] * math.sin(angle))
    a21 = (Axis[0] * Axis[1] * (1 - math.cos(angle))) + (Axis[2] * math.sin(angle))
    a22 = (Axis[1] * Axis[1] * (1 - math.cos(angle))) + (math.cos(angle))
    a23 = (Axis[1] * Axis[2] * (1 - math.cos(angle))) - (Axis[0] * math.sin(angle))
    a31 = (Axis[0] * Axis[2] * (1 - math.cos(angle))) - (Axis[1] * math.sin(angle))
    a32 = (Axis[1] * Axis[2] * (1 - math.cos(angle))) + (Axis[0] * math.sin(angle))
    a33 = (Axis[2] * Axis[2] * (1 - math.cos(angle))) + (math.cos(angle))
    Matrix = numpy.array([[a11, a12, a13], [a21, a22, a23], [a31, a32, a33]])
    return Matrix


def RunningNewStart(Parameters, List):
    """Remembers globally, where the next amino acid should be placed"""
    Working = numpy.asarray(Parameters.RunningNewStart)
    Add = numpy.asarray(List)
    Working = Working + Add
    Parameters.RunningNewStart = Working

def FindVdW(Atom):
    periodicsystem=numpy.load(PATH+"AminoAcids/elements.npy")
    radius=0
    for item in periodicsystem:
        if Atom[0] in item[0]:
            radius=float(item[1])
    if radius>0:
        return radius
    else:
        return 1.3

def GetCutOff(Atom1, Atom2):
    if Config.boolcutoff==0:
        VDW1=FindVdW(Atom1)
        VDW2=FindVdW(Atom2)
        cutoff=0.6*(VDW1+VDW2)
        #print(cutoff)
    else:
        cutoff=1.3
    return cutoff

def CheckValue(i, AminoList):
    Center1 = AminoList[i].getcenter()
    Complete = PDB.CompleteList + AminoList
    Length = len(PDB.CompleteList)
    for l in range(0, i + Length):
        Center2 = Complete[l].getcenter()
        CenterDistance = numpy.linalg.norm(numpy.asarray(Center1) - numpy.asarray(Center2))
        if CenterDistance < 10:
            for m in range(1, len(AminoList[i].Coords)):
                for n in range(1, len(Complete[l].Coords)):
                    #print(AminoList[i].atoms[m]+" vs. "+Complete[l].atoms[n]+" with ",end='')
                    Distance = numpy.linalg.norm(
                        numpy.asarray(AminoList[i].Coords[m]) - numpy.asarray(Complete[l].Coords[n]))
                    if Distance < GetCutOff(AminoList[i].atoms[m],Complete[l].atoms[n]):
                        # print(str(i) + str(m) + " vs. " + str(l) + str(n) + " " + str(Distance))
                        return 0
    return 1


def PhiPsiCalculatorAmino2(Amino2, Amino1):
    """Using the Planize function, which already deals with the planes to calculate angles, the existing phi and psi angles are
    calculated. This can be used later, when specific angles are plugged in, to actually reach good angles."""
    C1N2 = numpy.asarray(Amino1.Coords[2]) - numpy.asarray(Amino2.Coords[0])
    CA2N2 = numpy.asarray(Amino2.Coords[1]) - numpy.asarray(Amino2.Coords[0])
    CA2C2 = numpy.asarray(Amino2.Coords[1]) - numpy.asarray(Amino2.Coords[2])
    normalC1N2CA2 = numpy.cross(C1N2, CA2N2)
    normalN2CA2C1 = numpy.cross(CA2N2, CA2C2)
    normalC1N2CA2 = normalC1N2CA2 / numpy.linalg.norm(normalC1N2CA2)
    normalN2CA2C1 = normalN2CA2C1 / numpy.linalg.norm(normalN2CA2C1)
    Amino2.phiold = math.degrees(math.acos(numpy.dot(normalN2CA2C1, normalC1N2CA2)))
    C3 = Amino2.getNext()
    C2Newstart = numpy.asarray(Amino2.Coords[2]) - numpy.asarray(C3)
    normalCA2C2N3 = numpy.cross(CA2C2, C2Newstart)
    normalCA2C2N3 = normalCA2C2N3 / numpy.linalg.norm(normalCA2C2N3)
    Amino2.psiold = math.degrees(math.acos(numpy.dot(normalN2CA2C1, normalCA2C2N3)))


def Planize(Amino2, Amino1):
    """Planize is meant to turn the amino acid, which is to be added (Amino2) in a way, that the plane is
    established with the previous amino acid (Amino1). Please note, that C1 refers to C in Amino1 and CA2 is CA in
    Amino2. To do this, Amino2 is artifically placed at the newstart spot to get the direction vectors for all needed
    line segments. Then firstly the angle C1-N2-CA2 is fixed to be 109 degrees. Afterwards the plane O1-C1-N2 and
    C1-N2-CA2 is considerered, their respective normal vectors are calculated and their angle towards each other is
    taken. This angle will be the turn angle along the C1-N2 line to move CA2 into the plane. Now it is checked,
    if CA2 is turned to the right side or if it should be turned for another 180 degrees. The right side is being
    determined by CA2 having a shorter distance to O1."""
    # Move N2 to position:
    Amino2.move(Parameters.RunningNewStart, 0)
    # Turn Amino2 along the normal to C1N2 and N2CA2 to fit the angle C1N2CA1
    C1N2 = numpy.asarray(Amino1.Coords[2]) - numpy.asarray(Amino2.Coords[0])
    CA2N2 = numpy.asarray(Amino2.Coords[1]) - numpy.asarray(Amino2.Coords[0])
    Skalar = numpy.dot(C1N2, CA2N2)
    Lengthproduct = numpy.linalg.norm(C1N2) * numpy.linalg.norm(CA2N2)
    AngleC1N2CA1 = math.degrees(math.acos(Skalar / Lengthproduct))
    Axis = numpy.cross(C1N2, CA2N2)
    Axis = Axis / numpy.linalg.norm(Axis)
    Matrix = Drehmatrix(Axis, 109 - AngleC1N2CA1)
    Amino2.move(Amino2.Coords[0], 1)
    for i in range(1, len(Amino2.Coords)):
        Amino2.Coords[i] = list(numpy.dot(Matrix, Amino2.Coords[i]))
    Amino2.move(Parameters.RunningNewStart, 0)
    C1N2 = numpy.asarray(Amino1.Coords[2]) - numpy.asarray(Amino2.Coords[0])
    CA2N2 = numpy.asarray(Amino2.Coords[1]) - numpy.asarray(Amino2.Coords[0])
    O1C1 = numpy.asarray(Amino1.Coords[3]) - numpy.asarray(Amino1.Coords[2])
    N2C1 = numpy.asarray(Amino2.Coords[0]) - numpy.asarray(Amino1.Coords[2])
    normalC1N2CA2 = numpy.cross(C1N2, CA2N2)
    normalO1C1N2 = numpy.cross(O1C1, N2C1)
    normalC1N2CA2 = normalC1N2CA2 / numpy.linalg.norm(normalC1N2CA2)
    normalO1C1N2 = normalO1C1N2 / numpy.linalg.norm(normalO1C1N2)
    Skalarprodukt = numpy.dot(normalO1C1N2, normalC1N2CA2)
    Angle = math.degrees(math.acos(Skalarprodukt))
    #Angle = Angle # 180 - Angle
    Axis = C1N2 / numpy.linalg.norm(C1N2)
    Amino2.move(Amino2.Coords[0], 1)
    ##"""
    WAmino1 = []
    WAmino2 = []
    for i in range(0, len(Amino2.Coords)):
        WAmino1.append((0, 0, 0))
        WAmino2.append((0, 0, 0))
    Matrix1 = Drehmatrix(Axis, Angle)
    for i in range(1, len(Amino2.Coords)):
        WAmino1[i] = numpy.dot(Matrix1, Amino2.Coords[i])
    Matrix2 = Drehmatrix(Axis, Angle + 180)
    for i in range(1, len(Amino2.Coords)):
        WAmino2[i] = numpy.matmul(Matrix2, Amino2.Coords[i])
    L1 = numpy.linalg.norm(numpy.asarray(Amino1.Coords[3]) - numpy.asarray(WAmino1[1]))
    L2 = numpy.linalg.norm(numpy.asarray(Amino1.Coords[3]) - numpy.asarray(WAmino2[1]))
    if L1 <= L2:
        Amino2.Coords = WAmino1
    else:
        Amino2.Coords = WAmino2
    Amino2.move(Amino2.Coords[0], 1)
    PhiPsiCalculatorAmino2(Amino2, Amino1)
    return Amino2


def Indexadjustment(filename):
    catpdb = open(filename, "r")
    Lines = catpdb.readlines()
    newlines = []
    for i in range(0, len(Lines)):
        if "ATOM" in Lines[i][:6]:
            InIndex = str(i + 1)
            if i > 99999:
                Hex = str(hex(i + 1))
                InIndex = Hex[2:]
            if i > 1048575:
                InIndex = "*****"
            while len(InIndex) < 5:
                InIndex = " " + InIndex
            newlines.append("ATOM  " + InIndex + Lines[i][11:])
    catpdb.close()
    now=time.perf_counter()
    timer=round(now-Parameters.starttime,2)
    newcat = open(filename, "w")
    newcat.write("This is the structure for " + Parameters.name + " in Run " + str(Parameters.step) + " of Pep McConst; produced after "+str(timer)+" second(s)."+"\n")
    for i in range(0, len(newlines)):
        newcat.write(newlines[i])
    newcat.write("END")
    newcat.close()

def FromPDB(ChainList, StructureList):
    """From the inputed list of Amino Acids, the  list of Amino objects is constructed."""
    Parameters.RunningNewStart = Parameters.pdbstart
    Parameters.linenumber=0
    AminoList = []
    for i in range(0, len(ChainList)):
        AminoList.append(Amino(ChainList[i]))  # See Amino Init for clarification
        AminoList[i].structure = StructureList[i]
        if i == 0:
            AminoList[i].first = 1
    """An empty output file is created and closed."""
    Outputname = "Output/" + Parameters.name + ".pdb"
    Output = open(Outputname, "w")
    Output.close()
    """To circumvent overwrite issues, the output file is reopened with only the append option"""
    Output = open(Outputname, "a")
    """If one adds to a PDB the old PDB is copied to the new file here:"""
    if Parameters.addpdb == 1:
        Output.write(PDB.lines[0])
        for i in range(1, len(PDB.lines)):
            if "ATOM" in PDB.lines[i][:6]:
                if " OT1 "in PDB.lines[i]:
                    Constructor=PDB.lines[i].split(" OT1 ")
                    Build=Constructor[0]+" O   "+Constructor[1]
                    Parameters.linenumber=Parameters.linenumber+1
                    linenumber=str(Parameters.linenumber)
                    while len(linenumber)<8:
                        linenumber=" "+linenumber
                    Build=Build[:72]+linenumber+"\n"
                    Output.write(Build)
                elif " OT2 " in PDB.lines[i] or " OX2" in PDB.lines[i]:
                    pass
                else:
                    Parameters.linenumber=Parameters.linenumber+1
                    linenumber=str(Parameters.linenumber)
                    while len(linenumber)<8:
                        linenumber=" "+linenumber
                    Output.write(PDB.lines[i][:72]+linenumber+"\n")
    """Here the heart of the script happens, when amino acids are placed in 3d space with a randomization of the
    phi and psi angles, as well as a random rotation of the side chain. A detailed description can be found with the 
    single methods. These methods are repeated for every amino acid."""
    for i in range(0, len(AminoList)):
        """First we turn the next Amino Acid, so not the first one, so
        O_Previous-C_Previous-N_Next-CA_Next 
        are placed in their plane"""
        if AminoList[i].first == 1 and Parameters.addpdb == 1:
            AminoList[i] = Planize(AminoList[i], PDB.Last)
        if AminoList[i].first != 1:
            AminoList[i] = Planize(AminoList[i], AminoList[i - 1])
        """Then we randomize the angles"""
        AminoList[i].Alter()
        """Then we move the whole new amino acid to the right location"""
        AminoList[i].move(Parameters.RunningNewStart, 0)
        """and save what the new right location for the next one will be"""
        RunningNewStart(Parameters, AminoList[i].Newstart)
        """Update the PDB Lines and the Resid"""
        AminoList[i].writelines()
        AminoList[i].writeresid(i + 1)
        """Check for overlaps in the acid chain and abandons the run, if bad"""
        Parameters.Value = CheckValue(i, AminoList)
        if Parameters.Value == 0:
            break
        """Write the output file"""
        AminoList[i].WriteCoords(Output)
        AminoList[i].processed = 1
    Parameters.RunningNewStart = Parameters.pdbstart
    Output.write("END")
    Output.close()
    Indexadjustment("Output/" + Parameters.name + ".pdb")
    """If the whole run was good, the output file is copied with the run number, so it will not be overwritten
    by the next run"""
    if Parameters.Value == 1:
        shutil.copy("Output/" + Parameters.name + ".pdb",
                    "Output/" + Parameters.name + "_run" + str(Parameters.step) + ".pdb")
        outputname = Parameters.name + "_run" + str(Parameters.step) + ".pdb"
        if Parameters.psf == 1:
            Mini(outputname)
        Parameters.Traj = Parameters.Traj + 1
    """The working pdb file is removed, to not have the last pdb double (might be important for statistics)"""
    os.remove(Outputname)


"""
The Program starts here by gathering input. And writing the aquired data either to the Parameters class or establishing 
a list of Amino Objects. Additionally, you can choose, if you wish to run a Monte Carlo or a Las Vegas approach.
The Script will warn of a possible extensive run time.
"""
options, config = getopt.getopt(sys.argv[1:], "c:", ["config="])

if len(config)>1:
    print("Multiple Config Files have been given, exiting")
    quit()

Config=Configuration(config[0])
cwd=os.getcwd()
print(cwd)

#####################################################################################################
PATH=os.getcwd()+"/" #"/data/programs/pepmcconst/"#######################################################
#####################################################################################################

if not os.path.exists(PATH+"AminoAcids"):
    print("The required data for this script is not at the right location! Check your files. Exiting...")
    exit()
if not os.path.exists("Output"):
    os.system("mkdir Output")
Parameters = Parameters()

"""These are the ruins of the former input. They were deleted from here to be replaced with the config file input system"""

Parameters.name=Config.name
Parameters.addpdb=Config.add
PDB=PDB()
if Parameters.addpdb==1:
    Parameters.pdb=Config.structure
    PDB.fill()
Parameters.ResChain=Config.chain
Parameters.StructureList=Config.secondary
Parameters.chain=Config.chainnumber
if Config.vegas==1:
    Parameters.MonteCarlo=0
Parameters.steps=Config.runs

DisableWarning=0
AreYouSure = 1
counter = 0


if Parameters.MonteCarlo == 1:
    while Parameters.step <= Parameters.steps:
        print("Starting run " + str(Parameters.step) + " of " + str(Parameters.steps) + "...")
        FromPDB(Parameters.ResChain, Parameters.StructureList)
        Parameters.step = Parameters.step + 1
else:
    while AreYouSure == 1:
        print("Starting run " + str(Parameters.step) + ".")
        FromPDB(Parameters.ResChain, Parameters.StructureList)
        Parameters.step = Parameters.step + 1
        counter = counter + 1
        if DisableWarning == 1:
            if counter % 10000 == 0:
                print("Your LasVegas approach has now run for " + str(counter) + " runs!")
                Sure = input("Do you really wish to continue? (yes,no)")
                if Sure not in ("yes", "Yes", "y", "Y"):
                    AreYouSure = 0
        Success = str(Parameters.Traj / (Parameters.step - 1))
        if Parameters.Traj == 1:
            print("So far " + str(
                Parameters.Traj) + " structure has been written successfully. Success rate: " + Success)
        else:
            print("So far " + str(
                Parameters.Traj) + " structures have been written successfully. Success rate: " + Success)
        if Parameters.Traj == Parameters.steps:
            break
if Parameters.Traj == 1:
    print("1 structure has been written to Output/")
else:
    print(str(Parameters.Traj) + " structures have been written to Output/")


