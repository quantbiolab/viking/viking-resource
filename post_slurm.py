from argparse import ArgumentParser
from datetime import datetime, timedelta
import json
from logging import getLogger
import os
from subprocess import PIPE, run
import traceback
import yaml
import requests

from viking_resource import S3
from viking_resource.config import Config
from viking_resource.jobs import fill_file_path, load_program_data, update_job_status, upload_files
from viking_resource.storage import normalize_storage_path

logger = getLogger('viking')

def main(config: Config, jobid):
    url = f'https://{config.api_server}/async-function/user/{config.api_username}/project/{config.api_project}/functionid/{jobid}'
    headers = {'Authorization': f'Bearer {config.api_token}'}

    job_info = requests.get(url, headers=headers).json()
    
    # Cancel job if still running
    slurm_id = None
    if 'slurm_id' in job_info and job_info['slurm_id']:
    
        slurm_id = job_info['slurm_id']
    
        slurm_info = run(['sacct', '-j', str(slurm_id), '-X', '-o', 'State', '--parsable'], stdout=PIPE, text=True).stdout.split('\n')[1].strip().split('|')
    
        if slurm_info[0] in ['PENDING', 'RUNNING', 'SUSPENDED']:
            run(['scancel', str(slurm_id)])

    job_args_json: dict = json.loads(' '.join(job_info['args']))
    
    program_data = load_program_data(job_args_json['program'], config, job_args_json.get('program_cmd', None))
    
    files = [job_args_json['slurm_args']['stdout'], job_args_json['slurm_args']['stderr']]
    files = [os.path.join(job_args_json['cwd'], file) for file in files]
    
    for file in program_data['upload']:
        if 'format_fields' in program_data:
            files.append(os.path.join(job_args_json['task_dir'], fill_file_path(file, program_data['format_fields'], job_args_json['format_data'])))
        else:
            files.append(os.path.join(job_args_json['task_dir'], file))

    upload_files(files, config)

    # Collection job reason/exit code
    if slurm_id is None:
        state = -2
        reason = 'Job not submitted'
    else:
        slurm_info = run(['sacct', '-j', str(slurm_id), '-X', '-o', 'State,Reason,Elapsed', '--parsable'], stdout=PIPE, text=True).stdout.split('\n')[1].strip().split('|')    

        logger.debug(f"Job info: {slurm_info}")
            
        if slurm_info[0] == 'COMPLETED':
            state = 4
            reason = ''
        elif slurm_info[0] == 'FAILED':
            state = -1
            reason = slurm_info[1]
        elif 'CANCELLED' in slurm_info[0]:
            state = -2
            reason = slurm_info[0]
        elif slurm_info[0] == 'TIMEOUT':
            t = datetime.strptime(slurm_info[2], "%H:%M:%S")
            elapsed = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
            partition = json.loads(job_info['args'])['slurm_args']['partition']
            max_time =  json.loads(run(['scontrol', 'show', 'partition', partition, '--json'], stdout=PIPE, text=True).stdout)['partitions'][0]['maximums']['time']['number']
            if max_time * 60 < elapsed.total_seconds():
                state = -4
                reason = ''
            else:
                state = -3
                reason = ''
        else:
            state = 0
            reason = ' - '.join(slurm_info[0:2])
            
    update_job_status(jobid, state, reason, config)
        
    
if __name__ == '__main__':
    parser = ArgumentParser()
    
    parser.add_argument('config_file')
    parser.add_argument('job_id', type=int)

    args = parser.parse_args()
    
    config = Config(args.config_file)
    try:
        main(config, args.job_id)
    except Exception as e:
        logger.error(f'Program post_slurm failed with exception {e}')
        logger.debug(traceback.format_exc())

    