from argparse import ArgumentParser
import json
import logging
import os
import sys

from viking_resource import S3, normalize_storage_path, Config

logger = logging.getLogger('viking')

def main(config: Config, code, folder):
    if os.path.exists(folder):
        logger.error(f'Folder {folder} already exists, cannot initialize viking here.')
        return -1
    
    ## Create folder locally
    os.makedirs(folder)

    s3 = S3(config)
    
    programs = []
    
    for program in os.listdir(config.commands_path):
        if os.path.exists(os.path.join(config.commands_path, program, 'run.yaml')):
            programs.append(program)
            
    programs = json.dumps(programs)    
    
    s3.connection.put_object(Bucket=s3.bucket, Key=normalize_storage_path(os.path.join(folder, '.programs')), Body=bytes(programs, 'utf-8'))
    
    s3.connection.put_object(Bucket=s3.bucket, Key=normalize_storage_path(os.path.join(folder, '.verify')), Body=bytes(code, 'utf-8'))

if __name__ == '__main__':
    logger.debug(sys.argv)
    
    parser = ArgumentParser()

    parser.add_argument('config_file')
    parser.add_argument('code')
    parser.add_argument('folder')

    args = parser.parse_args()
    config = Config(args.config_file)

    sys.exit(main(config, args.code, args.folder))