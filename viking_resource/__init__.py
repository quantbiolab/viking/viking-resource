import logging

logging.basicConfig(
    level=logging.WARNING, 
    filename='viking.log', 
    filemode='a',
    format='%(asctime)s - %(levelname)s - %(name)s\n  %(message)s',)

logger = logging.getLogger('viking')
logger.setLevel(logging.DEBUG)

from .storage import *
from .config import *