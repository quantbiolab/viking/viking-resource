import json

class Config:
    api_username: str           # API access
    api_token: str
    api_project: str
    api_server: str
    s3_access_key: str          # S3 access
    s3_secret_key: str
    s3_server: str
    s3_bucket: str
    commands_path: str          # Path to commands folder
    slurm_defaults_path: str    # Path to SLURM defaults folder
    programs_path: str          # VIKING_ROOT
    hpc: str                    # Which cluster are we on
    
    def __init__(self, config_file_path) -> None:
        with open(config_file_path, 'r') as f:
            config = json.load(f)
            
        self.api_username = config['username']
        self.api_token = config['token']
        self.api_project = config['project']
        self.api_server = config['api_server']
        self.s3_access_key = config['s3_access_key']
        self.s3_secret_key = config['s3_secret_key']
        self.s3_server = config['s3_server']
        self.s3_bucket = config['s3_bucket']
        self.commands_path = config['commands_path']
        self.slurm_defaults_path = config['slurm_defaults_path']
        self.programs_path = config['programs_path']
        self.hpc = config['hpc']