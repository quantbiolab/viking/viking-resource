
import boto3
import botocore
import os

from .config import Config

from logging import getLogger

logger = getLogger('viking')

def normalize_storage_path(path):
    'taken from zarr.utils'
    # handle bytes
    if isinstance(path, bytes):
        path = str(path, 'ascii')
    # ensure str
    if path is not None and not isinstance(path, str):
        path = str(path)
        
    user = os.getlogin()
    if not path.startswith(user):
        path = user + '/' + path
        
    if path:
        # convert backslash to forward slash
        path = path.replace('\\', '/')
        # ensure no leading slash
        while len(path) > 0 and path[0] == '/':
            path = path[1:]
        # ensure no trailing slash
        while len(path) > 0 and path[-1] == '/':
            path = path[:-1]
        # collapse any repeated slashes
        previous_char = None
        collapsed = ''
        for char in path:
            if char == '/' and previous_char == '/':
                pass
            else:
                collapsed += char
            previous_char = char
        path = collapsed
        # don't allow path segments with just '.' or '..'
        segments = path.split('/')
        if any(s in {'.', '..'} for s in segments):
            raise ValueError("path containing '.' or '..' segment not allowed")
        # Remove any references to environment variables by removing the dollar sign
        path = path.replace('$', '')
    else:
        path = ''
    return path

class S3:
    def __init__(self, config: Config) -> None:
        self.bucket = config.s3_bucket
        self.connection = boto3.client('s3', 
                                       aws_access_key_id=config.s3_access_key, 
                                       aws_secret_access_key=config.s3_secret_key,
                                       endpoint_url='https://' + config.s3_server)
        
    def upload(self, path):
        if os.path.isdir(path):
            for root, dirs, files in os.walk(path):
                for file in files:
                    file_path = os.path.join(root, file)
                    remote_file = normalize_storage_path(file_path)
                    self.connection.upload_file(file_path, self.bucket, remote_file)
        else:
            remote_path = normalize_storage_path(path)
            self.connection.upload_file(path, self.bucket, remote_path)

    def _ls(self, path):
        "list objects in the bucket"
        path = normalize_storage_path(path)
        objs = self.connection.list_objects(Bucket=self.bucket, Prefix=path)
        names = [o['Key'] for o in objs.get('Contents', [])]
        is_truncated = objs.get('IsTruncated', False)

        while is_truncated:
            marker = objs.get('NextMarker', '')
            objs = self.connection.list_objects(Bucket=self.bucket, Prefix=path, Marker=marker)
            names.extend([o['Key'] for o in objs['Contents']])
            is_truncated = objs.get('IsTruncated', None)
        
        names = [file for file in names if file != path]
        return names

    def download(self, path, overwrite=False):
        remote_path = normalize_storage_path(path)
        is_dir = False
        try:
            obj = self.connection.head_object(Bucket=self.bucket, Key=remote_path)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                logger.debug(f"Object {remote_path} does not exist, treating as directory")
                is_dir = True
            else:
                logger.error(f"Error {e.response['Error']['Code']} while downloading {remote_path}")

        if is_dir:
            os.makedirs(path, exist_ok=True)
            objects = self._ls(remote_path)
            for obj in objects:
                local_file = os.path.join(path, obj[len(remote_path)+1:])
                os.makedirs(os.path.dirname(local_file), exist_ok=True)
                if os.path.exists(local_file):
                    if overwrite:
                        os.remove(local_file)
                    else:
                        continue
                self.connection.download_file(self.bucket, obj, local_file)
        else:
            if os.path.exists(path):
                if overwrite:
                    os.remove(path)
                    self.connection.download_file(self.bucket, remote_path, path)
            else:
                self.connection.download_file(self.bucket, remote_path, path)