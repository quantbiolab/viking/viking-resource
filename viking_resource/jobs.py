import json
from logging import getLogger
import os

import yaml

from viking_resource.storage import S3, normalize_storage_path


logger = getLogger('viking')

def update_job_status(jobid, state, reason, config):
    logger.debug(f"State: {state}, Reason: {reason}")
    
    s3 = S3(config)
    
    state_reason_json = json.dumps({state: state, reason: reason})
    
    s3.connection.put_object(Bucket=s3.bucket, Key=normalize_storage_path(os.path.join(config.api_username, config.api_project, 'jobs', str(jobid))), Body=bytes(state_reason_json, 'utf-8'))


def upload_files(files, config):
    logger.debug(f"Uploading files: {files}")
    
    s3 = S3(config)
    for file in files:
        try:
            s3.upload(file)
        except FileNotFoundError:
            logger.error(f"File {file} not found, skipping upload.")
            
def load_program_data(program, config, cmd = None):

    program_slug = program

    with open(os.path.join(config.commands_path, program_slug, 'run.yaml')) as f:
        program_data = yaml.safe_load(f)

    if cmd:
        program_data = program_data[cmd]
    
    return program_data

def fill_file_path(path: str, format_fields: list[str], format_data: dict):
    for field in format_fields:
        path = path.replace('{' + field + '}', str(format_data[field]))
        
    return path