#!/bin/bash

while [[ $# -gt 0 ]]; do
  case $1 in
    -u|--user)
      USERNAME="$2"
      shift
      shift
      ;;
    -p|--path)
      VIKING_INSTALL_PATH="$2"
      shift
      shift
      ;;
    --hpc)
      HPC="$2"
      shift
      shift
      ;;
    --config)
      CONFIG="$2"
      shift
      shift
      ;;
    --default)
      DEFAULT=YES
      shift
      ;;
    --help)
      echo "Usage: install.sh -u <username (optional)> -p <install-path (optional)> --hpc <hpc> --config <config>"
      exit 0
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      shift
      ;;
  esac
done

if [ -z $HPC ]; then
  echo "HPC not set"
  exit 1
fi

if [ -z $CONFIG ]; then
  echo "Config not set"
  exit 1
fi

case $HPC in
  glogin|nhr-goe)
    BASE_PATH="/user"
    HPC_NAME="glogin"
    module load python
    module load git
    ;;
  glogin-legacy|nhr-goe-legacy)
    BASE_PATH="/home"
    HPC_NAME="glogin"
    module load python
    module load git
    ;;
  qblcluster|qbl)
    BASE_PATH="/user"
    HPC_NAME="qblcluster"
    ;;
  *)
    echo "Unknown HPC $HPC"
    exit 1
    ;;
esac

if [ -z $USERNAME ]; then
  echo "Assuming installation for $USER"
  HOME_PATH=$HOME
else
  HOME_PATH=$BASE_PATH/$USERNAME
fi

if [ -z ${VIKING_INSTALL_PATH+x} ]; then
    VIKING_INSTALL_PATH=$HOME_PATH/.viking
fi

export VIKING_INSTALL_PATH

FUNCTIONS_PATH=$HOME_PATH/functions
VIKING_PROGRAMS_PATH=$VIKING_INSTALL_PATH/programs
VIKING_FUNCTIONS_PATH=$VIKING_INSTALL_PATH/functions
HPCSerA_AGENT_PATH=$HOME_PATH/HPCSerA_Agent
export VIKING_CONFIG_FILE=$VIKING_INSTALL_PATH/config.json

echo "Installing Viking to $VIKING_INSTALL_PATH"

git clone --quiet https://gitlab.uni-oldenburg.de/quantbiolab/viking/viking-resource.git $VIKING_INSTALL_PATH

export VIKING_VENV_PATH=$VIKING_INSTALL_PATH/venv

echo "Installing packages to $VIKING_VENV_PATH" 

python -m venv $VIKING_VENV_PATH
source $VIKING_VENV_PATH/bin/activate
pip install --quiet -r $VIKING_INSTALL_PATH/requirements.txt

echo "Copying functions to $FUNCTIONS_PATH"

mkdir -p $FUNCTIONS_PATH

for runscript in hash.sh viking.sh verify.sh post_slurm.sh update.sh; do
    envsubst < $VIKING_FUNCTIONS_PATH/$runscript > $FUNCTIONS_PATH/$runscript
    chmod +x $FUNCTIONS_PATH/$runscript
done

mkdir -p $VIKING_PROGRAMS_PATH

## Creating config file
echo "Creating config file in $VIKING_INSTALL_PATH/config.json"

python - << EOF
import json
data = json.load(open("$CONFIG"))
keys_not_found = []
for key in ["username", "token", "project", "api_server", "s3_access_key", "s3_secret_key", "s3_server", "s3_bucket"]:
    if key not in data:
        keys_not_found.append(key)
if keys_not_found:
    print(f"Missing keys {keys_not_found} in the config file")
    exit(1)
data["hpc"] = "$HPC_NAME"
data["commands_path"] = "$VIKING_INSTALL_PATH/commands/tasks"
data["programs_path"] = "$VIKING_PROGRAMS_PATH"
data["slurm_defaults_path"] = "$VIKING_INSTALL_PATH/slurm_defaults"
with open("$VIKING_INSTALL_PATH/config.json", "w") as f:
    json.dump(data, f)
EOF

## Installing viking programs

echo "Installing HPCSerA Agent to $HPCSerA_AGENT_PATH"

git clone https://gitlab-ce.gwdg.de/hnolte1/HPCSerA-Agent.git $HPCSerA_AGENT_PATH

echo "Run the following command in the background repeatedly (i.e. in a cron job)"
echo ""
echo "cd $HPCSerA_AGENT_PATH && source $VIKING_VENV_PATH/bin/activate && python agent.py $VIKING_INSTALL_PATH/config.json"
