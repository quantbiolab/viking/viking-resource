# viking-resource

This is a repository for the code needed by the online platform [viking](https://viking-suite.com/) on a resource controlled via [HPCSerA](https://hpcsera.hpc.gwdg.de/web/). 

## Installation

[Contact nhr-support](mailto:nhr-support@gwdg.de) for activating HPCSerA for your account at NHR@Göttingen. Provide your account name and project name when contacting. They will set up the viking resource code and the HPCSerA agent in your home directory to continuosly ask the HPCSerA server for any new jobs. They will also reply to you with your username, token and project id for HPCSerA, which you can then use in viking to set up a NHR resource.
