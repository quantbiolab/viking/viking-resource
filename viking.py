from argparse import ArgumentParser
import json
from logging import getLogger
import os
import shutil
from subprocess import PIPE, run
import traceback
#from viking.cleanup import cleanup
#from viking.data import S3
#from viking.runners import SLURM, Local
from yaml import safe_load

import sys

import os

from viking_resource import S3
from viking_resource.config import Config
from viking_resource.jobs import fill_file_path, load_program_data, upload_files

logger = getLogger('viking')


def main(config: Config, args):
    data: dict = json.loads(args)

    program_data = load_program_data(data['program'], config, data.get('program_cmd', None))
        
    # assume that if function requires hash, files have already been downloaded for hash computation
    if 'hash' not in program_data:
        os.makedirs(data['cwd'], exist_ok=True)

        s3 = S3(config)

        if 'download' in program_data:
            for file in program_data['download']:
                if 'format_fields' in program_data:
                    file = fill_file_path(file, program_data['format_fields'], data['format_data'])
                path = os.path.join(data['task_dir'], file)
                s3.download(path, overwrite=True)
        else:
            s3.download(data['task_dir'], overwrite=True)

    runscript = os.path.join(config.commands_path, data['program'], program_data['runscript'])

    # Local execution on the frontend node
    if 'local' in program_data and program_data['local']:
        process = run([runscript, *data['args']], cwd=data['cwd'], stdout=PIPE, stderr=PIPE, text=True)
        
        logger.debug(f'stdout: {process.stdout}')
        
        if process.returncode:
            logger.warning(f"Error running command {data['program']}: {process.stderr}")
            
        files = []
        for file in program_data['upload']:
            if 'format_fields' in program_data:
                files.append(os.path.join(data['task_dir'], fill_file_path(file, program_data['format_fields'], data['format_data'])))
            else:
                files.append(os.path.join(data['task_dir'], file))
        
        upload_files(files, config)
        
        return

    with open(os.path.join(config.slurm_defaults_path, config.hpc + '.yaml')) as f:
        slurm_defaults = safe_load(f)

    slurm_args = data['slurm_args']

    for key, value in slurm_defaults.items():
        if key not in slurm_args and value is not None:
            slurm_args[key] = value

    sbatch_args = ['sbatch']

    if 'account' in slurm_args:
        sbatch_args.extend(['-A', slurm_args['account']])
    
    if 'stdout' in slurm_args:
        sbatch_args.extend(['-o', slurm_args['stdout']])
    
    if 'stderr' in slurm_args:
        sbatch_args.extend(['-e', slurm_args['stderr']])

    if 'jobid' in slurm_args:
        sbatch_args.extend(['-J', slurm_args['jobid']])

    sbatch_args.extend([
            '--parsable',
            '-p', slurm_args['partition'], 
            '-t', slurm_args['time'], 
            '-N', str(slurm_args['nodes']), 
            '-n', str(slurm_args['ntasks_per_node']), 
            '-c', str(slurm_args['ncpus']), 
            f'--export=VIKING_ROOT={config.programs_path}',
            runscript, *data['args']])

    env = dict(os.environ, VIKING_ROOT = config.programs_path)

    logger.debug(f'running command: {sbatch_args}')

    process = run(sbatch_args, stdout=PIPE, stderr=PIPE, cwd=data['cwd'], env=env, text=True)

    if process.returncode != 0:
        raise RuntimeError(f'Error running sbatch: {process.stderr}')
        
    jobid = int(process.stdout.split(';')[0].strip())

    print(jobid)

        
if __name__ == '__main__':
    config_file = sys.argv[1]
    arg_json = ' '.join(sys.argv[2:])

    logger.debug(arg_json)

    config = Config(config_file)

    try:
        main(config, arg_json)
    except Exception as e:
        logger.error(f'Program viking failed with exception {e}')
        logger.debug(traceback.format_exc())

