#!/bin/bash

echo "Updating Viking" >> viking.log

export VIKING_INSTALL_PATH=$1
VIKING_FUNCTIONS_PATH=$VIKING_INSTALL_PATH/functions
FUNCTIONS_PATH=$HOME/functions
export VIKING_VENV_PATH=$VIKING_INSTALL_PATH/venv
export VIKING_CONFIG_FILE=$VIKING_INSTALL_PATH/config.json

for runscript in hash.sh viking.sh verify.sh post_slurm.sh update.sh; do
    envsubst < $VIKING_FUNCTIONS_PATH/$runscript > $FUNCTIONS_PATH/$runscript
    chmod +x $FUNCTIONS_PATH/$runscript
done

source $VIKING_VENV_PATH/bin/activate
pip install --quiet -U -r $VIKING_INSTALL_PATH/requirements.txt