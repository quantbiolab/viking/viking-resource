#!/bin/bash                                                                             
##############################################################################################
# 
#   This is the runscript for dcd2vtb on NHR@Göttingen.
#   Usage:
#   ~~~~
#   dcd2vtb -input <file.dcd> -output <file.vtb>
#   ~~~~
#
##############################################################################################

INPUT_FILES=""
OUTPUT_FILE=""
ARGS=""

## Read command-line parameters
while test ${#} -gt 0
do
    if [ "$1" == "-output" ]; then
        shift
        OUTPUT_FILE="$1"
    elif [ "$1" == "-input" ]; then
        shift
        INPUT_FILES="$INPUT_FILES $1"
    else
        ARGS="$ARGS $1"
    fi
    shift
done

$VIKING_ROOT/bin/dcd2vtb $INPUT_FILES $OUTPUT_FILE

echo "0"

