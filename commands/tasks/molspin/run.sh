#!/bin/bash                                                                             
##############################################################################################
#   ! This is an auto-generated script for VIKING  (https://viking-suite.com).
#   ! Please do not remove or modify it!
# 
#   This is the runscript for MolSpin on Abacus 2.0
#   Usage:
#   ~~~~
#   molspin  [-ppn NN] [-time "00:30:00"] -queue QUEUE -input "jobname.msd"
#       -outlog "stdout.log" -errlog "stderr.log" -outfile "molspin.log"
#   ~~~~
#
#   The output must be the job id number in STDOUT, or an error message in STDERR.
##############################################################################################

INPUT_FILE=""
LOGFILE="MOLSPIN_$$_OUT.log"
ERRFILE="MOLSPIN_$$_ERR.log"
OPTION_P=""
OPTION_O=""
OPTION_A=""
OPTION_Z=""
OPTION_T=""
OPTION_R=""
OPTION_C=""

######## Read command-line parameters: ########
while test ${#} -gt 0
do
    if [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    elif [ "$1" == "-p" ]; then
        shift
        OPTION_P="-p $1"
    elif [ "$1" == "-o" ]; then
        OPTION_O=$1
    elif [ "$1" == "-a" ]; then
        OPTION_A=$1
    elif [ "$1" == "-z" ]; then
        OPTION_Z=$1
    elif [ "$1" == "-t" ]; then
        OPTION_T=$1
    elif [ "$1" == "-r" ]; then
        shift
        OPTION_R="-r $1"
    elif [ "$1" == "-c" ]; then
        shift
        OPTION_C="-c $1"
    fi
    shift
done

if [ "$INPUT_FILE" == "" ]; then
    echo "No input file specified."
    exit 1
fi

MOLSPIN_COMMANDLINE_OPTIONS="$OPTION_A $OPTION_C $OPTION_O $OPTION_P $OPTION_R $OPTION_T $OPTION_Z"

module load intel-oneapi-mkl
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$VIKING_ROOT/lib

$VIKING_ROOT/bin/molspin $MOLSPIN_COMMANDLINE_OPTIONS "$INPUT_FILE"

