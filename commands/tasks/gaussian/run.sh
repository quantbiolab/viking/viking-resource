#!/bin/bash                                                                             
##############################################################################################
#   ! This is an auto-generated script for VIKING  (https://viking-suite.com).
#   ! Please do not remove or modify it!
# 
#   This is the runscript for Gaussian16 on HLRN P3
#   Usage:
#   ~~~~
#   g09 -input "jobname.gjf"
#       -outlog "stdout.log" -errlog "stderr.log" -outfile "gaussian.log"
#   ~~~~
#
#   The output must be the job id number in STDOUT, or an error message in STDERR.
##############################################################################################

INPUT_FILE=""
OUTPUT_FILE=""
ARGS=""

######## Read command-line parameters: ########
while test ${#} -gt 0
do
    if [ "$1" == "-outfile" ]; then
        shift
        OUTPUT_FILE=$1
    elif [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    else
        ARGS="$ARGS $1"
    fi
    shift
done

if [ "$INPUT_FILE" == "" ]; then
    echo "No input file specified."
    exit 1
fi

module purge
module load gaussian

g16 "$INPUT_FILE" "$OUTPUT_FILE"
