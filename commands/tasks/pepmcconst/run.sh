#!/bin/bash

while test ${#} -gt 0              
do
    if [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    else
        ARGS="$ARGS $1"
    fi
    shift
done

if [ "$INPUT_FILE" == "" ]; then
    echo "No config file specified."
    exit 1
fi

env

python3 $VIKING_ROOT/bin/peptidemontecarloconstructor.py "$INPUT_FILE"
