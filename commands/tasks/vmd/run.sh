#!/bin/bash

#to run this script do ./run_NAMD_2_13_HLRN.sh to see the options
#an example input would be ./run_NAMD_2_13_HLRN.sh example 10 standard96 00:10:00  

INPUT_FILE=""
ARGS=""

######## Read command-line parameters: ########
while test ${#} -gt 0              
do
    if [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    else
        ARGS="$ARGS $1"
    fi
    shift
done

#LOGFILE="$INPUT_FILE.$QUEUE$NUMNODES.log.txt"
if [ "$INPUT_FILE" == "" ]; then
    echo "No input file specified."
    exit 1
fi

export PREFERRED_SOFTWARE_STACK=nhr-tmod
source /etc/profile

export SLURM_CPU_BIND=none

module purge
module load vmd 

mkdir -p tmp/tmpwat
vmd -dispdev none -e $INPUT_FILE
