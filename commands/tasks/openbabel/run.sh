#!/bin/bash
##############################################################################################
#   ! This is an auto-generated script for VIKING  (https://viking-suite.com).
#   ! Please do not remove or modify it!
#
#   This is the runscript for openbabel on Abacus
#   Usage:
#   ~~~~
#   openbabel.sh -rootdir "/path/to/viking/root/"  <openbabel args...>
#   ~~~~
#
#   All output to STDOUT is ignored. If the program fails with a non-zero exitcode,
#   STDERR is parsed for error messages.
#
#   NOTE: unlike other programs used in VIKING, openbabel is currently run on the
#   front-end node, instead of adding it to the queue on Abacus
##############################################################################################

INPUT_FILE=""
OUTPUT_FILE=""
VIKING_ROOT=""
INPUT_FORMAT=""
OUTPUT_FORMAT=""
FIRST=""
LAST=""

## Read command-line parameters
while test ${#} -gt 0
do
    if [ "$1" == "-rootdir" ]; then
        shift
        VIKING_ROOT="$1"
    elif [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    elif [ "$1" == "-output" ]; then
        shift
        OUTPUT_FILE="$1"
    elif [ "$1" == "-input_format" ]; then
        shift
        INPUT_FORMAT="-i$1"
    elif [ "$1" == "-output_format" ]; then
        shift
        OUTPUT_FORMAT="-o$1"
    elif [ "$1" == "-first" ]; then
        shift
        FIRST="-f$1"
    elif [ "$1" == "-last" ]; then
        shift
        LAST="-l$1"
    fi
    shift
done

module load openbabel
obabel $INPUT_FORMAT $INPUT_FILE $OUTPUT_FORMAT $OUTPUT_FILE $FIRST $LAST > /dev/null

echo "0"
