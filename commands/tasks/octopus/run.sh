#!/bin/bash

echo "Not Available as of now on p3"
exit -1

INPUT_FILE=""
ARGS=""

######## Read command-line parameters: ########
while test ${#} -gt 0              
do
    if [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    else
        ARGS="$ARGS $1"
    fi
    shift
done

if [ -f $LOGFILE ]; then
  echo "Octopus log file $LOGFILE already exists!"
  exit -1
fi

module purge
module load Octopus/13.0-foss-2022b

# For pinning OpenMP threads correctly:
export OMP_NUM_THREADS=46
export OMP_PLACES=cores

# MPI can not use an infinity band
export FI_PROVIDER=tcp

mpirun octopus

