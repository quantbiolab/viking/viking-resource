#!/bin/bash
INPUT_FILE=""
ARGS=""

######## Read command-line parameters: ########
while test ${#} -gt 0              
do
    if [ "$1" == "-input" ]; then
        shift
        INPUT_FILE=$1
    else
        ARGS="$ARGS $1"
    fi
    shift
done

if [ "$INPUT_FILE" == "" ]; then
    echo "No config file specified."
    exit 1
fi


######## Make virtual environment if it does not exisit ########
if [ ! -d "$VIKING_ROOT/bin/cubeDog_Venv/" ]; then
	module -q purge
	module -q load python/3.11.6 
	python -m venv $VIKING_ROOT/bin/cubeDog_Venv/
	source $VIKING_ROOT/bin/cubeDog_Venv/bin/activate
	pip install -q --upgrade pip
	pip install -q -r $VIKING_ROOT/bin/cubeDogSrc/requirements.txt
fi

module purge
module load Python/3.11.6 

source $VIKING_ROOT/bin/cubeDog_Venv/bin/activate

python3 $VIKING_ROOT/bin/cubeDog.py -i $INPUT_FILE -p 46
