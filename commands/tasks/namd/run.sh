#!/bin/bash                                                                             
##############################################################################################
#   ! This is an auto-generated script for VIKING  (https://viking-suite.com).
#   ! Please do not remove or modify it!
# 
#   This the runscript for Namd on the HLRN.
#   Usage:
#   ~~~~
#   namdXX  [-nodes NN]  [-ppn NN]  [-time "00:30:00] -queue QUEUE
#       -input "file.conf" -outlog "stdout.log" -errlog "stderr.log"
#       [ +idlepoll ... (other parameters for Namd)]
#   ~~~~
#
#   The output must be the job id number in STDOUT, or an error text in STDERR.
##############################################################################################


CONFFILE=""

######## Read command-line parameters: ########
while test ${#} -gt 0              
do
    if [ "$1" == "-input" ]; then
        shift
        CONFFILE=$1
    else
        ARGS="$ARGS $1"
    fi
    shift
done


if [ ! -f $CONFFILE ]; then
  echo "NAMD input file $CONFFILE does not exist!"
  exit -1
fi

export SLURM_CPU_BIND=none

module purge
#module load intel-oneapi-mpi
module load openmpi
module load namd/3.0.1-smp

mkdir -p output

mpiexec namd3 +p192 +setcpuaffinity $CONFFILE
