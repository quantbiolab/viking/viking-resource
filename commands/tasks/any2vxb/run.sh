#!/bin/bash                                                                             
##############################################################################################
#   ! This is an auto-generated script for VIKING  (https://viking-suite.com).
#   ! Please do not remove or modify it!
# 
#   This is the runscript for any2vxb on the Abacus 2.0 supercomputer.
#   Usage:
#
#      any2vxb -input <input.xyz|pdb|psf|dcd> [ -input <input2.xyz|pdb|dcd> ... ] \
#           -output <file.vsb> -output <file.vtb> -rootdir "/path/to/viking/root/"
#
#   All output to STDOUT is ignored. If the program fails with a non-zero
#   exit code, STDERR is parsed for error messages.
#
#   NOTE: unlike most other programs used in VIKING, any2vxb is currently run
#   on the front-end node, instead of adding it to the queue on Abacus
##############################################################################################

INPUT_FILES=""
OUTPUT_FILES=""
ARGS=""

## Read command-line parameters
while test ${#} -gt 0
do
    if [ "$1" == "-output" ]; then
        shift
        OUTPUT_FILES="$OUTPUT_FILES $1"
    elif [ "$1" == "-input" ]; then
        shift
        INPUT_FILES="$INPUT_FILES $1"
    else
        ARGS="$ARGS $1"
    fi
    shift
done

any2vxb $INPUT_FILES $OUTPUT_FILES > /dev/null

echo "0"

