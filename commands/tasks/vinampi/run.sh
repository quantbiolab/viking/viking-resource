#!/bin/bash
#############################################################################
#   ! This is an auto-generated script for VIKING  (https://viking-suite.com).
#   ! Please do not remove or modify it!
# 
#   This the runscript for VinaMPI on Abacus 2.0
#   Usage:   vinampiXX  [-nodes NN]  [-ppn NN]  [-time "00:30:00] -queue QUEUE
#       -receptors-in "TXT_file_with_receptors" -ligands-in "txt_file_with_ligands"
#       -receptors-dir "/path/to/receptor/dir" -ligands-out "/path/to/ligand/dir"
#       -outlog "stdout.log" -errlog "stderr.log"
#
#   The output must be the job id number in STDOUT, or an error message in STDERR.
#############################################################################

ARGS=""
RECEPTORS_IN=""
LIGANDS_IN=""
RECEPTORS_DIR=""
LIGANDS_DIR=""

######## Read command-line parameters: ########
while test ${#} -gt 0
do
    if [ "$1" == "-receptors-in" ]; then
        shift
        RECEPTORS_IN=$1
    elif [ "$1" == "-ligands-in" ]; then
        shift
        LIGANDS_IN=$1
    elif [ "$1" == "-receptors-dir" ]; then
        shift
        RECEPTORS_DIR=$1
    elif [ "$1" == "-ligands-dir" ]; then
        shift
        LIGANDS_DIR=$1
    elif [ "$1" == "-rootdir" ]; then
        shift
        VIKING_ROOT="$1"
    else
        ARGS="$ARGS $1"
    fi
    shift
done

if [ "$RECEPTORS_IN" == "" ] || [ "$RECEPTORS_DIR" == "" ] || [ "$LIGANDS_IN" == "" ] || [ "$LIGANDS_DIR" == "" ]; then
    echo "Missing input file / directory for receptors / ligands." 1>&2
    exit 1
fi

###########################################

NUM_RECEPTORS=`cat $RECEPTORS_IN | grep receptor | wc -l | tr -d "\n"`
NUM_LIGANDS=`cat $LIGANDS_IN | wc -l | tr -d "\n"`

###########################################

module load gcc
module load openmpi 
module load openbabel

###########################################################################
### The script to be run in 'srun':
###########################################################################
$VIKING_ROOT/bin/VinaMPI  $RECEPTORS_IN $LIGANDS_IN $NUM_RECEPTORS $NUM_LIGANDS $RECEPTORS_DIR $LIGANDS_DIR $VIKING_ROOT/bin/vina/vina

 # Find 10 best (energy-wise) dockings and convert the resulting structures to
 # PDB (from PDBQT):
for i in \\\`for f in out/input/*; do A="\\\$(head -n2 \\\$f | tail -n1 | cut -d " " -f9-10)" ; echo \\\$A \\\$f; done | grep '^[-0-9]' | sort -n | head -n 10 | awk '{print \\\$1 "   " \\\$2  > "ener.dat"; print \\\$2}'\\\`
do
    j=\\\`echo \\\$i | perl -pe 's#^.+?input_(.+?)\\\.pdbqt\\\$#\\\$1#i'\\\`
    obabel -ipdbqt \\\$i -opdb -O \\\$j.pdb -f1 -l 1
done
