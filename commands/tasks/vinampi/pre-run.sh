
ARGS=""
RECEPTORS_IN=""
LIGANDS_IN=""
RECEPTORS_DIR=""
LIGANDS_DIR=""

######## Read command-line parameters: ########
while test ${#} -gt 0
do
    if [ "$1" == "-receptors-in" ]; then
        shift
        RECEPTORS_IN=$1
    elif [ "$1" == "-ligands-in" ]; then
        shift
        LIGANDS_IN=$1
    elif [ "$1" == "-receptors-dir" ]; then
        shift
        RECEPTORS_DIR=$1
    elif [ "$1" == "-ligands-dir" ]; then
        shift
        LIGANDS_DIR=$1
    elif [ "$1" == "-rootdir" ]; then
        shift
        VIKING_ROOT="$1"
    else
        ARGS="$ARGS $1"
    fi
    shift
done

if [ "$RECEPTORS_IN" == "" ] || [ "$RECEPTORS_DIR" == "" ] || [ "$LIGANDS_IN" == "" ] || [ "$LIGANDS_DIR" == "" ]; then
    echo "Missing input file / directory for receptors / ligands." 1>&2
    exit 1
fi

###########################################################################
### KLUDGE:  Fix pdbqt files that could possibly be garbled by MGLTools:
###########################################################################
cat > "fix-pdbqt.py" << PY

from sys import argv
from re import compile as re_compile

 # Match the position of the decimal delimiter (dot) in the x-coordinate value:
REGEX_SHIFT = re_compile( r'^(?:ATOM..|HETATM).{24}\s*([^.]+\.)' )

fname = argv[1]

with open( fname, 'r' ) as f :
    for s in f :
        match = REGEX_SHIFT.search( s )
        if match :
            dot_pos = match.end( 1 )
            offset = dot_pos - 35
            if offset > 0 :
                substr = s[17+offset:]
                s = s[:17] + substr
        print( s, end="" )
PY

cp $RECEPTORS_DIR/input.pdbqt $RECEPTORS_DIR/maybe_wrong.pdbqt
python3 fix-pdbqt.py $RECEPTORS_DIR/maybe_wrong.pdbqt > $RECEPTORS_DIR/input.pdbqt
