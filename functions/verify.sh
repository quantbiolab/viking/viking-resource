#!/bin/bash

source $VIKING_VENV_PATH/bin/activate

cd $VIKING_INSTALL_PATH

python verify.py $VIKING_CONFIG_FILE $@